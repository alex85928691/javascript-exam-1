import { addSerialNumber, halfNumbers, spliceNames } from "../src/map";

describe("array map test", () => {
  // Please add test cases here
  test('half',()=>{
    const pp = [4,8,6];

    const result = halfNumbers(pp);

    expect(result).toEqual([2,4,3]);


  })

  test('splice',()=>{
    const pk = ['lan','ad'];

    const result1 = spliceNames(pk);

    expect(result1).toEqual(['Hello lan','Hello ad']);
  })

  test('serial',()=>{
    const fruit = ['a','b'];

    const result2 = addSerialNumber(fruit);

    expect(result2).toEqual(['1. a','2. b']);

  })
});
