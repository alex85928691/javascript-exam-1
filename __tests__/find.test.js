import { firstGrownUp, firstOrange, firstLengthOver5Name } from "../src/find";

describe("array find test", () => {
  // Please add test cases here
  test('firstgronup',()=>{
    const first = [17,19];

    const result = firstGrownUp(first);

    expect(result).toEqual(19);


  })

  test('firstOr',()=>{

    const orange = ['apple','orange'];

    const result1 = firstOrange(orange);

    expect(result1).toEqual('orange');

  })

  test('firstle',()=>{
    const length = ['asddasd','asd'];

    const result2 = firstLengthOver5Name(length);

    expect(result2).toEqual('asddasd');


  })
});
