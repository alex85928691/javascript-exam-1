import { filterEvenNumbers, filterLengthWith4, filterStartWithA } from '../src/filter';

describe('array filter test', () => {
    test('filterEvenNumbers returns only even numbers', () => {
        const numbers = [2, 5, 4, 3, 7];
        const result = filterEvenNumbers(numbers);
      
        expect(result).toEqual([2, 4]);
    });

    test('filterLengthWith4 returns only strings with length 4', () => {
        const words = ['abcd', 'abcde', 'x'];
        const result = filterLengthWith4(words);
      
        expect(result).toEqual(['abcd']);
    });
    
    test('filterStartWithA returns only strings that start with "a" or "A"', () => {
        const letters = ['ab', 'b', 'abc', 'd'];
        const result = filterStartWithA(letters);
      
        expect(result).toEqual(['ab', 'abc']);
    });
});